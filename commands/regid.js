var setId = require('./id').set;

function irc(from, to, param, client) {
	if(!param || !/\d+/.test(param)) {
		client.say(to, from + ', pfv digite ".regid idNoErepublik"');
		return; 
	}
	param = parseInt(param, 10);
	client.once('notice', function(ns, me, text, message) {
		var registeredNick = 'NickServ' === ns && (('STATUS ' + from + ' 3') === text);
		setId(from, 
			{
				id: param, 
				irc: registeredNick
			}, function(error){
				if(error) {
					client.say(to, from + ', não foi possível registrar a ID ' + param + ' para o seu nick ):');
				} else {
					client.say(to, param + ' registrado com sucesso para ' + from + ' (:');
				}
		});
	});
    client.say('nickserv', 'STATUS ' + from);
	return null;
}

function telegram(from, to, param, bot) {
	//TODO
}

module.exports = {
	irc : irc,
	telegram : telegram
};